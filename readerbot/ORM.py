# This file is part of PyReader Bot project
# File is using to build an object-relation mapping of database via
# SQLAlchemy for the best interaction with data
#
# Copyright (c) 2017, Evgeniy Privalov, https://www.facebook.com/eugenprivalov

import os
import psycopg2
import datetime
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, JSON, Boolean, ForeignKey, DateTime
from sqlalchemy.orm import relation, sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

DATABASE = os.getenv("DATABASE", "pyreader")
# USERNAME = os.getenv("USERNAME", "pyreader")  # "Uncomment on Production
USERNAME = "pyreader"  # Remove on Production
# PASSWORD = os.getenv("PASSWORD", "pyreader")  # Uncomment on Production
PASSWORD = "pyreader"  # Remove on Production

ENGINE = create_engine('postgresql://{}:{}@localhost:5432/{}'.format(USERNAME, PASSWORD, DATABASE))


class Users(Base):
    """
    User object
    """
    __tablename__ = "tg_bot_users"
    id = Column(Integer, unique=True, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, default=None, nullable=True)
    username = Column(String, nullable=False)
    phone = Column(String, default=None, nullable=True)
    email = Column(String, default=None, nullable=True)
    active = Column(Boolean, default=False)

    group_id = Column(Integer, ForeignKey("tg_bot_groups.id"))
    group = relation("Groups", backref='tg_bot_groups', lazy=False)

    def __init__(self, id, first_name, last_name, username, phone, email, active):
        """
        User constructor

        :param first_name: First name from Telegram response
        :param last_name: Last name from Telegram response
        :param username: Username from Telegram response
        :param phone:
        :param email:
        :param active:
        """
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.phone = phone
        self.email = email
        self.active = active

    def __repr__(self):
        """

        :return: String to easy-understandable which object were used
        """
        return "<User('{}', '{}', '{}'>".format(self.id, self.first_name, self.username)


class Groups(Base):
    """
    Groups of users
    Groups:
        default - every users on creating and change in database before
        admins - users which have special grants
    """
    __tablename__ = "tg_bot_groups"
    id = Column(Integer, primary_key=True)
    group = Column(String)

    def __init__(self, group):
        """
        Group object constructor
        :param group:
        """
        self.group = group

    def __repr__(self):
        """
        Group object representation method

        :return: String to easy-understandable which object were used
        """
        return "<Group '{}'>".format(self.group)


class Forms(Base):
    """
    Forms to contain questions for users
    """
    __tablename__ = "tg_forms"
    id = Column(Integer, primary_key=True)
    data = Column(JSON)

    def __init__(self, data):
        """

        :param data:
        """
        self.data = data

    def __repr__(self):
        """
        Form representation method
        :return: String to easy-understandable which object were used
        """
        return "<Form '{}'>".format(self.id)


class Answers(Base):
    """

    """
    __tablename__ = "tg_answers"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("tg_bot_users.id"))
    answer = Column(JSON)

    user = relation("Users", backref='tg_answers', lazy=False)

    def __init__(self, user_id, answer):
        """

        :param user_id:
        :param answer:
        """
        self.user_id = user_id,
        self.answer = answer

    def __repr__(self):
        """
        Answers representational method

        :return: String to easy-understandable which object were used
        """
        return "<Answer '{}'>".format(self.user_id)


class Logs(Base):
    """
    All logs of actions in bot and any other
    """
    __tablename__ = "logs"
    id = Column(Integer, primary_key=True)
    logs = Column(String)
    log_date = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, logs):
        """
        Logs constructor
        :param logs:
        """
        self.logs = logs

    def __repr__(self):
        """
        Logs representational method

        :return: String to easy-understandable which object were used
        """
        return "<Logs '{}'>".format(self.id)


class Broadcasts(Base):
    __tablename__ = "tg_bot_broadcasts"
    id = Column(Integer, primary_key=True, unique=True)
    group_id = Column(Integer, ForeignKey("tg_bot_groups.id"), unique=False)
    broadcast_date = Column(DateTime, default=datetime.datetime.utcnow)
    broadcast_data = Column(JSON)
    sent = Column(Boolean, default=False)
    group = relationship("Groups", backref="tg_bot_broadcasts", lazy=False)

    def __init__(self, broadcast_data, group_id):
        """
        Broadcast constructor

        :param broadcast_data: JSON-formatted data with broadcast content
        :param group_id: Group identification to determine users
                which will be in list of broadcasting
        """
        self.broadcast_data = broadcast_data
        self.group_id = group_id

    def __repr__(self):
        """
        Broadcasts representational method

        :return: String to easy-understandable which object were used
        """
        return "<Broadcasts '{}'>".format(self.id)


class States(Base):
    """
    States object to save current user's state to
    determine next action as easy as it possible
    """
    __tablename__ = "tg_bot_states"
    id = Column(Integer, primary_key=True, unique=True)
    user_id = Column(Integer, ForeignKey("tg_bot_users.id"))
    user = relation("Users", backref="tg_bot_states", lazy=True)
    state = Column(String)

    def __init__(self, user_id, state):
        """
        States constructor

        :param user_id: Unique Telegram identification of user
        :param state: One of the possible states
        """
        self.user_id = user_id
        self.state = state

    def __repr__(self):
        """
        States representational method

        :return: String to easy-understandable which object were used
        """
        return "<States '{}'>".format(self.id)


# Build all tables
# Uncomment on creating database schema
# Base.metadata.create_all(ENGINE)
