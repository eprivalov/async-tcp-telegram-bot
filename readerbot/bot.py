# Part of the PyReader Bot project
# File is using to interact with Telegram API and
# accept connections from the clients via TCP protocol
# to make a broadcasts for group of users
#
# Copyright 2017 (c) Evgeniy Privalov, https://www.facebook.com/eugenprivalov

import os
import re
import uuid
import json
import asyncio
import requests
import logging
from datetime import datetime as dt
from base64 import b64decode
from mimetypes import guess_extension
from aiotg import Bot, Chat
from decorators import *
from ORM import ENGINE, Users, Groups, Forms, Answers, Logs, States, Broadcasts
from sqlalchemy.orm import sessionmaker
from sqlalchemy import update, desc

Session = sessionmaker(bind=ENGINE)
session = Session()

logger = logging.getLogger(__name__)
API_TOKEN = os.getenv("BOT_API_TOKEN", "470067871:AAHvw_D1Cuis9dro3uveYh9Yk22dwpdmzPA")

bot = Bot(api_token=API_TOKEN)

admins_markup = json.dumps({
    "keyboard": [["Сделать рассылку"], ["Информация"]],
    "resize_keyboard": True, "one_time_keyboard": False
})

users_markup = json.dumps({
    "keyboard": [["Информация"]],
    "resize_keyboard": True, "one_time_keyboard": False
})


@bot.command(r"Информация")
async def info(chat, match):
    user = session.query(Users).filter_by(id=chat.sender["id"]).first()
    if user.group_id == 1:
        markup = users_markup
    else:
        markup = admins_markup
    await chat.send_text(chat=chat, text="Информация", reply_markup=markup)


@bot.command(r'/start')
async def start(chat, match):
    """
    Handling /start message sent by user (on activation or any other situations)

    :param chat:
    :param match:
    :return:
    """
    # Finding user in database
    users = session.query(Users.id, Users.active, Users.group_id).filter_by(id=chat.sender["id"])
    if users.scalar() is None:
        # Create new user instance
        user = Users(
            id=chat.sender["id"],
            username=chat.sender["username"],
            first_name=chat.sender["first_name"],
            last_name=chat.sender["last_name"],
            phone=None,
            email=None,
            active=False,
            group="default"
        )
        # Create new user state instance
        new_user_state = States(user_id=chat.sender["id"], state="main_menu")
        # Create new row of user state in database
        session.add(new_user_state)
        # Create new row of user in database
        session.add(user)
        # Database commit
        session.commit()
    else:
        user = users.first()
        # Access denied if user is not activated
        if not user.active:
            await chat.send_text(chat=chat, keyboard={}, text="Hello. You have no access!")
            return 0
        # Find user state
        user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
        user_state.state = "main_menu"
        session.commit()

    message = "Hello, {}!".format(chat.sender["username"])
    if user.group_id == 2:
        markup = admins_markup
    else:
        markup = users_markup
    await chat.send_text(chat=chat, reply_markup=markup, text=message)


@bot.command(r"Сделать рассылку")
@check_user_group
async def make_broadcast(chat, match):
    """

    :param chat:
    :param match:
    :return:
    """
    kb = [["Администраторам"], ["Пользователям"]]
    markup = json.dumps({
        "keyboard": kb,
        "resize_keyboard": True,
        "one_time_keyboard": False
    })
    message = "Кому делаем рассылку?"
    await chat.send_text(chat=chat, reply_markup=markup, text=message)


@bot.command(r"(Администраторам|Пользователям)")
@check_user_group
async def make_broadcast_to_admins(chat, match):
    """

    :param chat:
    :param match:
    :return:
    """
    group_matched = match
    group = "admins"
    if group_matched == "Пользователям":
        group = "admins"
    elif group_matched == "Администраторам":
        group = "default"
    # Get user state to determine future reactions
    user_state = session.query(States).filter_by(user_id=chat.sender["id"])
    # Select admin group id by name 'admins'
    admin_group_id = session.query(Groups).filter_by(group=group).first().id
    if user_state.scalar() is not None:
        # Update user state to fill broadcast data
        user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
        user_state.state = "broadcast"
    else:
        # Create user state to determine different interactions
        new_state = States(user_id=chat.sender["id"], state="broadcast")
        session.add(new_state)

    new_broadcast = Broadcasts(group_id=admin_group_id, broadcast_data={"data": []})
    session.add(new_broadcast)
    session.commit()

    # Compile a keyboard markup
    kb = [["Отправить"], ["Отмена"]]
    markup = json.dumps({
        "keyboard": kb,
        "resize_keyboard": True,
        "one_time_keyboard": False
    })
    message = "Для наполнения рассылки контентом, просто присылайте мне сообщения"
    await chat.send_text(chat=chat, reply_markup=markup, text=message)


@bot.command(r"Отмена")
async def cancel(chat, match):
    """

    :param chat:
    :param match:
    :return:
    """
    user = session.query(Users).filter_by(id=chat.sender["id"]).first()
    user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
    user_state.state = "main_menu"
    session.commit()

    if user.group_id == 2:
        markup = admins_markup
    else:
        markup = users_markup
    await chat.send_text(chat=chat, reply_markup=markup, text="Операция отменена")


@bot.command(r"Отправить")
@check_user_group
async def make_broadcast(chat, match):
    """

    :param chat:
    :param match:
    :return:
    """
    user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
    broadcast = session.query(Broadcasts).order_by(desc(Broadcasts.id)).first()
    users = session.query(Users).filter_by(group_id=broadcast.group_id).all()
    for user in users:
        data = json.loads(broadcast.broadcast_data)["data"]
        for i in data:
            if i["type"] == "text":
                await bot.send_message(chat_id=user.id, text=i["message"])
            elif i["type"] == "photo":
                await bot.send_photo(chat_id=user.id, photo=i["photo"])
            elif i["type"] == "document":
                await bot.send_document(chat_id=user.id, document=i["document"], file_name=i["file_name"])
            elif i["type"] == "location":
                await bot.send_location(chat_id=user.id, latitude=i["latitude"], longitude=i["longitude"])

    broadcast.sent = True
    user_state.state = "main_menu"
    session.commit()
    markup = json.dumps({"keyboard": [["Сделать рассылку"]], "resize_keyboard": True})
    await chat.send_text(chat=chat, text="Рассылка выполнена", reply_markup=markup)


@bot.handle("photo")
async def photo_handler(chat, photo):
    """

    :param chat:
    :param photo:
    :return:
    """
    photo_id = sorted(photo, key=lambda item: item["width"], reverse=True)[0]["file_id"]
    user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
    if user_state.state == "broadcast":
        broadcast = session.query(Broadcasts).filter_by(sent=False).order_by(desc(Broadcasts.id)).first()
        try:
            data = json.loads(broadcast.broadcast_data)
        except TypeError:
            data = broadcast.broadcast_data

        data["data"].append({
            "type": "photo",
            "photo": photo_id
        })
        broadcast.broadcast_data = json.dumps(data)
        session.commit()

        kb = [["Отправить"]]
        markup = json.dumps({
            "keyboard": kb,
            "resize_keyboard": True,
            "one_time_keyboard": False
        })
        await chat.send_text(chat=chat, reply_markup=markup, text="Что-нибудь ещё или отправляем?")
    else:
        await chat.send_text(chat=chat, text="Пам парам пам пам")


@bot.handle("location")
async def location_handler(chat, location):
    """

    :param chat:
    :param location:
    :return:
    """
    user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
    if user_state.state == "broadcast":
        broadcast = session.query(Broadcasts).filter_by(sent=False).order_by(desc(Broadcasts.id)).first()
        try:
            data = json.loads(broadcast.broadcast_data)
        except TypeError:
            data = broadcast.broadcast_data

        data["data"].append({
            "type": "location",
            "latitude": location["latitude"],
            "longitude": location["longitude"]
        })
        broadcast.broadcast_data = json.dumps(data)
        session.commit()

        kb = [["Отправить"]]
        markup = json.dumps({
            "keyboard": kb,
            "resize_keyboard": True,
            "one_time_keyboard": False
        })
        await chat.send_text(chat=chat, reply_markup=markup, text="Что-нибудь ещё или отправляем?")
    else:
        await chat.send_text(chat=chat, text="Пам парам пам пам")


@bot.handle("document")
async def document_handler(chat, document):
    """

    :param chat:
    :param document:
    :return:
    """
    user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
    if user_state.state == "broadcast":
        broadcast = session.query(Broadcasts).filter_by(sent=False).order_by(desc(Broadcasts.id)).first()
        try:
            data = json.loads(broadcast.broadcast_data)
        except TypeError:
            data = broadcast.broadcast_data

        data["data"].append({
            "type": "document",
            "document": document["file_id"],
            "file_name": document["file_name"]
        })
        broadcast.broadcast_data = json.dumps(data)
        session.commit()

        kb = [["Отправить"]]
        markup = json.dumps({
            "keyboard": kb,
            "resize_keyboard": True,
            "one_time_keyboard": False
        })
        await chat.send_text(chat=chat, reply_markup=markup, text="Что-нибудь ещё или отправляем?")
    else:
        await chat.send_text(chat=chat, text="Пам парам пам пам")


@bot.command(r"([\W\w\d]+)")
async def message_handler(chat, match):
    """
    Handle each messages which user was sent depends on current user state

    :param chat:
    :param match:
    :return:
    """
    message = match.group(1)
    user_state = session.query(States).filter_by(user_id=chat.sender["id"]).first()
    if user_state.state == "broadcast":
        broadcast = session.query(Broadcasts).filter_by(sent=False).order_by(desc(Broadcasts.id)).first()
        try:
            data = json.loads(broadcast.broadcast_data)
        except TypeError:
            data = broadcast.broadcast_data

        data["data"].append({"type": "text", "message": message})
        broadcast.broadcast_data = json.dumps(data)
        session.commit()

        kb = [["Отправить"]]
        markup = json.dumps({
            "keyboard": kb,
            "resize_keyboard": True,
            "one_time_keyboard": False
        })
        await chat.send_text(chat=chat, reply_markup=markup, text="Что-нибудь ещё или отправляем?")
    else:
        await chat.send_text(chat=chat, text="Пам парам пам пам")


@bot.callback(r"post__(.*)")
async def callback_handle(chat, cq, match):
    action_type = match.group(1)
    if action_type == "accept":
        new_log = Logs(logs="User has been accepted request to post")
        session.add(new_log)
        session.commit()

        # TODO: What to do?
        await chat.send_text(chat=chat, text="Операция подтверждена")
    else:
        await chat.send_text(chat=chat, text="Операция отменена")


class TgBot(object):
    """
    TgBot is object to interact with Telegram APO via TCP Socket
    """
    __slots__ = "bot"
    BOT = Bot(api_token=API_TOKEN)
    MEDIA_FOLDER = os.getcwd() + "/media/"

    def __init__(self, bot=None):
        """
        TgBot object send data to the target users

        :param bot: Instance of the Bot
        :param data: JSON-format data to sending to users
        """
        if bot is not None:
            self.bot = bot

    def get_and_send_broadcast(self, data):
        """

        :param data: JSON formatted data
        :return:
        """
        # Check if taken data is <dict>
        # If object is not JSON formatted then write error
        # to log database and exit of logic
        if isinstance(data, dict):
            # Make a content type accorded broadcast for each user
            try:
                is_to_post = True if len(data["message"]["answers"]) != 0 else False
            except KeyError:
                is_to_post = False

            # Get group name which will be used for the broadcasting
            target_group = data["group"]
            # Filter group id by name
            group = session.query(Groups.id).filter_by(group=target_group)
            if group.scalar() is not None:
                # Filtering users by group id if it exists
                users = session.query(Users.id).join(Groups). \
                    filter(Users.active is not True). \
                    filter(Groups.id == group.first().id).all()
                for user in users:
                    self.send_data(
                        user_id=user.id,
                        data=data,
                        is_to_post=is_to_post
                    )
            elif target_group == "all":
                users = session.query(Users).all()
                for user in users:
                    self.send_data(
                        user_id=user.id,
                        data=data,
                        is_to_post=is_to_post
                    )
            else:
                logger.info("No groups were found")
                new_log = Logs(log="No groups were found")
                session.add(new_log)
                session.commit()
                return 0
        else:
            # Write error to log database if object is not JSON formatted
            new_log = Logs(logs="Data is not a python dictionary or JSON instance")
            session.add(new_log)
            session.commit()
            return 0

    def send_data(self, user_id, data, is_to_post=None):
        """
        Method to determine type of content and do corresponding action

        :param is_to_post: Check if broadcasting need to have inline buttons
        :param user_id: Unique Telegram id for target user
        :param data: JSON formatted data with content to broadcasting
        :return:
        """
        markup = []
        if is_to_post:
            keyboard = [[
                dict(
                    text=i,
                    callback_data="post__{}".format("accept" if i == "Yes" else "decline")
                ) for i in data["message"]["answers"]
            ]]
            markup = json.dumps({"inline_keyboard": keyboard})

        message = data["message"]
        if message["type"] == "text":
            # Send Text message
            self.BOT.send_message(chat_id=user_id, text=message["text"], reply_markup=markup)
        elif message["type"] == "image":
            # Sending photo to user chat
            file_name = self.save_file(content=message["content"])

            extra_message = False
            text = message["text"]
            # Checking length of accompanying text
            if len(text) > 200:
                extra_message = True

            # Open file and send to user chat
            with open(file_name, "rb") as image:
                # Make a data that will be send
                data = {
                    "chat_id": user_id,
                    # Add caption if length of accompanying text is lower than 200 characters
                    "caption": text if not extra_message else None,
                    "reply_markup": markup
                }
                files = {"photo": image}
                # Make a POST request to Telegram
                requests.post(
                    "https://api.telegram.org/bot{}/sendPhoto".format(API_TOKEN),
                    data=data,
                    files=files
                )

                # Sends one more text message, if length of the accompanying text is grater than 200 characters
                if extra_message:
                    self.BOT.send_message(chat_id=user_id, text=text, reply_markup=markup)
        elif message["type"] == "document":
            # Send document with file name
            file_name = self.save_file(content=message["content"])

            extra_message = False
            text = message["text"]
            if len(text) > 200:
                extra_message = True

            with open(file_name, "rb") as file:
                data = {
                    "chat_id": user_id,
                    "caption": text if not extra_message else None,
                    "reply_markup": markup
                }
                files = {"document": file}
                requests.post(
                    "https://api.telegram.org/bot{}/sendDocument".format(API_TOKEN),
                    data=data,
                    files=files
                )
                if extra_message:
                    self.BOT.send_message(chat_id=user_id, text=text, reply_markup=markup)
        elif message["type"] == "location":
            # Send Map by latitude and longitude
            self.BOT.send_location(chat_id=user_id, latitude=message["latitude"], longitude=message["longitude"],
                                   reply_markup=markup)

    def check_media_folder(self, path):
        """
        Check if folder exists. If not - create

        :param path: Path of target folder of media
        :return: Boolean object
        """
        if not os.path.isdir(path):
            os.makedirs(path)
        return True

    def save_file(self, content):
        """
        Save image to server that has been sent via TCP as Base64

        :param content: String object, base64 formatted file
        :return: String with name of created file
        """
        mime_type = content.split(";base64")[0].split("data:")[1]
        extension = guess_extension(mime_type)
        file_id = uuid.uuid5(uuid.NAMESPACE_DNS, "{}".format(int(dt.now().timestamp()))).hex
        self.check_media_folder(path=self.MEDIA_FOLDER)
        file_name = "{}{}{}".format(self.MEDIA_FOLDER, file_id, extension)
        t = open(file_name, "wb+")
        t.write(b64decode(re.sub(r"^data:[\w\/]+;base64,", "", content)))
        t.close()

        return file_name
