# This file is part of PyReader Bot project
# File is using to create a TCP protocol asynchronously server
# to accept connections and make broadcasts and
# create a Telegram API update listener to interact with Telegram BOT
#
# Copyright (c) 2017, Evgeniy Privalov, https://www.facebook.com/eugenprivalov

import os
import sys
import logging
import asyncio
from bot import bot
from ORM import ENGINE, Logs
from sqlalchemy.orm import sessionmaker
from server import ServerProtocol

Session = sessionmaker(bind=ENGINE)
session = Session()


async def start():
    """

    :return:
    """
    await bot.loop()


if __name__ == "__main__":
    try:
        loop = asyncio.get_event_loop()
        coro = loop.create_server(ServerProtocol, '127.0.0.1', 8888)
        loop.run_until_complete(coro)
        loop.run_until_complete(start())
        loop.close()
    except KeyboardInterrupt:
        pass
    except Exception as err:
        new_log = Logs(logs=err)
        session.add(new_log)
        session.commit()
        pass
