# Part of the Pyreader_bot project
# Used to test TCP Socket connection

# Copyright 2017 (c) Evgeniy Privalov, https://www.facebook.com/eugenprivalov

import json
import asyncio
import logging
from bot import TgBot
from ORM import ENGINE, Logs
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=ENGINE)
session = Session()
logger = logging.getLogger(__name__)


class EchoClientProtocol(asyncio.Protocol):
    def __init__(self, message, loop):
        self.message = message
        self.loop = loop

    def connection_made(self, transport):
        transport.write(self.message)
        transport.close()
        # self.send_signal_to_bot(data=self.message)

    def data_received(self, data):
        print('Data received: {!r}'.format(data.decode()))

    def connection_lost(self, exc):
        self.loop.stop()


loop = asyncio.get_event_loop()
message = {
    "timestamp": 1095357343.54,
    "id": 877994604561387500,
    "group": "all",
    "type": "answer",
    "message":
        {
            "text": "Welcome message",
            "type": "image",
            "answers":
                [
                    "Yes",
                    "No"
                ],
            "content": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH1ggDCwMADQ4NnwAAAFVJREFUGJWNkMEJADEIBEcbSDkXUnfSgnBVeZ8LSAjiwjyEQXSFEIcHGP9oAi+H0Bymgx9MhxbFdZE2a0s9kTZdw01ZhhYkABSwgmf1Z6r1SNyfFf4BZ+ZUExcNUQUAAAAASUVORK5CYII="
        },
}

message = json.dumps(message).encode("utf-8")
coro = loop.create_connection(lambda: EchoClientProtocol(message, loop), '127.0.0.1', 8888)
# loop.run_forever()
loop.run_until_complete(coro)
# loop.create_future()
# loop.run_forever()
loop.close()
