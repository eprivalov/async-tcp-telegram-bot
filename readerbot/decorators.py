# This file is part of PyReader Bot project
# This file is using to prepare decorators for the future using
#
# Copyright (c) 2017, Evgeniy Privalov, https://www.facebook.com/eugenprivalov

import functools
from ORM import ENGINE, Users, Groups, Forms, Answers, Logs, States, Broadcasts
from sqlalchemy.orm import sessionmaker
from sqlalchemy import update, desc
Session = sessionmaker(bind=ENGINE)
session = Session()


def check_user_group(func):
    """
    Method check does user exists in admin group to give access rights or not

    :param func:
    :return:
    """
    @functools.wraps(func)
    def inner(chat, match):
        user = session.query(Users).filter_by(id=chat.sender["id"]).first()
        group_id = session.query(Groups.id).filter_by(group="admins").first()
        if user.group_id != group_id:
            chat.send_text(chat=chat, text="Access denied")
        else:
            return func(chat, match)

    return inner
