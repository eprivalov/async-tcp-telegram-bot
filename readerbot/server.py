"""

"""
import json
import asyncio
import logging
from bot import TgBot
from ORM import ENGINE, Logs
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=ENGINE)
session = Session()
logger = logging.getLogger(__name__)


class ServerProtocol(asyncio.Protocol):

    def connection_made(self, transport):
        """

        :param transport:
        :return:
        """
        peername = transport.get_extra_info('peername')
        log = 'Connection from {}'.format(peername)
        new_log = Logs(logs=log)
        session.add(new_log)
        session.commit()

    def data_received(self, data):
        """

        :param data:
        :return:
        """
        message = json.loads(data.decode())
        self.send_signal_to_bot(data=message)

    @staticmethod
    def send_signal_to_bot(data):
        """

        :param data:
        :return:
        """
        try:
            isinstance(data, dict)
            if "message" in data.keys():
                a = TgBot()
                a.get_and_send_broadcast(data)
                return 0
            else:
                session.add(Logs(logs="Data object is not JSON formatted"))
                session.commit()
                pass
        except TypeError as err:
            session.add(Logs(logs=err))
            session.commit()
            pass
